#!/bin/bash

echo "Write key to file"
echo "$GC_KEY" > /key.json

echo "Login to Google Cloud with $GC_SERVICE_ACCOUNT"
gcloud auth activate-service-account "$GC_SERVICE_ACCOUNT" --key-file /key.json --project "$GC_PROJECT_ID"

echo "Set compute/zone to $GC_ZONE"
gcloud config set compute/zone "$GC_ZONE"

echo "Set project to $GC_PROJECT_ID"
gcloud config set project "$GC_PROJECT_ID"